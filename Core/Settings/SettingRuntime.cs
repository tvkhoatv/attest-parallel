﻿using NUnit.Framework;

namespace ATTest.Settings;

public sealed class SettingRuntime
{
    public SettingRuntime() {}

    public SettingRuntime(TestContext context) : this()
    {
        Context = context;
    }

    public static TestContext? Context { get; set; }

    public static SettingRuntime Instance => new Lazy<SettingRuntime>(() => new SettingRuntime()).Value;

    public static dynamic? GetProperty(string property)
    {
        //return TestContext.CurrentContext.Test.Properties[property] ?? null;
        return TestContext.Parameters[property] ?? null;
    }
}
