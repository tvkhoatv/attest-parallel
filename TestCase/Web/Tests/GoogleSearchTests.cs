﻿using NUnit.Framework;
using OpenQA.Selenium;
using ATTest.Drivers;
using ATTest.TestCase.Pages;

namespace ATTest.TestCase.Tests
{
    [TestFixture(BrowserType.Chrome)]
    [Parallelizable(ParallelScope.All)]
    public class GoogleSearchTests
    {
        BrowserType browser;
        public GoogleSearchTests(BrowserType type)
        {
            browser = type;
        }


        [SetUp]
        public void InitAutomationFramework()
        {
            DriverFactory.InitDriver(browser);
        }


        [Test]
        public void WhenEnterSearchTextInGoogleHomePageAndClickOnSearch_ThenSearchShouldDisplay()
        {
            //Google home page
            GoogleHomePage home = new GoogleHomePage(DriverFactory.GetDriver<IWebDriver>());
            home.Driver.Navigate().GoToUrl("https://google.com");
            home.EnterSearchKeyword("google");
            home.ClickSearch();
            //google search results page
            GoogleSearchResultsPage results = new GoogleSearchResultsPage(DriverFactory.GetDriver<IWebDriver>());
            results.VerifyResultLink("google");
        }

        [Test]
        public void WhenEnterSearchTextInGoogle()
        {
            //GoogleHomePage home = new GoogleHomePage(DriverFactory.GetDriver<IWebDriver>());
            //home.Driver.Navigate().GoToUrl("https://google.com");
            //home.EnterSearchKeyword("selenium auto c#");
            //home.ClickSearch();
            ////google search results page
            //GoogleSearchResultsPage results = new GoogleSearchResultsPage(DriverFactory.GetDriver<IWebDriver>());
            //results.VerifyResultLink("selenium auto c#");

            VnexpressPage vnexpresspage = new VnexpressPage(DriverFactory.GetDriver<IWebDriver>());
            vnexpresspage.Driver.Navigate().GoToUrl("https://vnexpress.net");
        }

        [TearDown]
        public void CleanBrowserSessions()
        {
            DriverFactory.CloseDriver();
        }
        

    }
}
