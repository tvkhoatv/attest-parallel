﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;

namespace ATTest.TestCase.Pages
{
    public class GoogleHomePage
    {
        private IWebDriver _driver;
        private const string SEARCH_TEXT_BOX_NAME = "q";
        private const string GOOGLE_SEARCH_BUTTON_NAME = "btnK";
        public IWebDriver Driver
        {
            get
            {
                return _driver;
            }

            set
            {
                _driver = value;
            }
        }

#pragma warning disable CS8618 // Non-nullable field must contain a non-null value when exiting constructor. Consider declaring as nullable.
        public GoogleHomePage(IWebDriver driver)
        {
            Driver = driver;
        }

        public IWebElement SearchField
        {
            get
            {
                return Driver.FindElement(By.Name(SEARCH_TEXT_BOX_NAME));
            }
        }

        public IWebElement SearchButton
        {
            get
            {
                return Driver.FindElement(By.Name(GOOGLE_SEARCH_BUTTON_NAME));
            }
        }

        public void EnterSearchKeyword(string keyword)
        {
            SearchField.SendKeys(keyword);
            SearchField.SendKeys(Keys.Escape);
        }

        public void ClickSearch()
        {
            SearchButton.Submit();
        }

    }
}
