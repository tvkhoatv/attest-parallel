﻿using ATTest.Drivers;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;

namespace ATTest.WebTesting
{
    public class ChromeDriverWeb : IDrivers
    {
        public string? FOLDERPATH { get; set; }

        public object? Driver { get; set; }

        public object DriverServices
        {
            get
            {
                ChromeDriverService service = ChromeDriverService.CreateDefaultService(FOLDERPATH);
                service.HideCommandPromptWindow = false;
                service.SuppressInitialDiagnosticInformation = true;
                return service;
            }
        }

        public object DesiredCapabilities
        {
            get
            {
                ChromeOptions opts = new ChromeOptions();
                return opts;
            }
        }

        public void InitDriver(Configuration config)
        {
            FOLDERPATH = config.DRIVER_EXE_FOLDER;
            config.DesiredCapabilities = config.DesiredCapabilities ?? DesiredCapabilities;
            config.DriverServices = config.DriverServices ?? DriverServices;
            IWebDriver driver = new ChromeDriver((ChromeDriverService)config.DriverServices, (ChromeOptions)config.DesiredCapabilities, TimeSpan.FromSeconds(60));
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromMilliseconds(config.PageLoadTimeout);
            //driver.Navigate().GoToUrl(config.START_URL);
            Driver = driver;
        }

    }
}
