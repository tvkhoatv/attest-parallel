﻿namespace ATTest.Drivers
{
    public enum BrowserType
    {
        Chrome,
        FireFox,
        InternetExplorer,
        PhantomJSBrowser,
        iOS,
        Android,
        Edge
    }
}
