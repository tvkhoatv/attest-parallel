﻿using OpenQA.Selenium;
using SeleniumExtras.PageObjects;

namespace ATTest.TestCase.Pages
{
    public class VnexpressPage
    {
        private IWebDriver _driver;
        public IWebDriver Driver
        {
            get
            {
                return _driver;
            }

            set
            {
                _driver = value;
            }
        }

#pragma warning disable CS8618 // Non-nullable field must contain a non-null value when exiting constructor. Consider declaring as nullable.
        public VnexpressPage(IWebDriver driver)
        {
            Driver = driver;
            PageFactory.InitElements(Driver, this);
        }
    }
}
