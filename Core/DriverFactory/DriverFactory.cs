﻿using ATTest.WebTesting;
using OpenQA.Selenium;

namespace ATTest.Drivers
{
    public class DriverFactory
    {
        private static ThreadLocal<object> storedDriver = new ThreadLocal<object>();

        public static DriverType GetDriver<DriverType>()
        {
            return (DriverType)DriverStored;
        }

        public static object? DriverStored
        {
            get
            {
                if (storedDriver == null)
                    return null;
                else
                    return storedDriver.Value;
            }
            set => storedDriver.Value = value;
        }

        public static void InitDriver(BrowserType browser, Configuration? config = null)
        {
            config = config ?? new Configuration();
            switch (browser)
            {
                case BrowserType.Chrome:
                    ChromeDriverWeb ChromedriverInstance = new ChromeDriverWeb();
                    ChromedriverInstance.InitDriver(config);
                    DriverStored = ChromedriverInstance.Driver;
                    break;

                case BrowserType.FireFox:
                    FireFoxDriverWeb FirefoxdriverInstance = new FireFoxDriverWeb();
                    FirefoxdriverInstance.InitDriver(config);
                    DriverStored = FirefoxdriverInstance.Driver;
                    break;

                case BrowserType.InternetExplorer:
                    InternetExplorerDriverWeb IEdriverInstance = new InternetExplorerDriverWeb();
                    IEdriverInstance.InitDriver(config);
                    DriverStored = IEdriverInstance.Driver;
                    break;

                //case BrowserType.Edge:
                //    InternetExplorerDriverWeb edgedriverInstance = new EdgeDriverWeb();
                //    iedriverInstance.InitDriver(config);
                //    DriverStored = iedriverInstance.Driver;
                //    break;
            }

        }

        public static void CloseDriver()
        {
            IWebDriver? driver = DriverStored as IWebDriver;
            driver?.Quit();
            DriverStored = null;
        }
    }
}