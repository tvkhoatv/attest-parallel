﻿using ATTest.Utilities;
using System.Reflection;

namespace ATTest.Drivers
{
    public class Configuration
    {
        public object? DesiredCapabilities;
        public object? DriverServices;
        public double PageLoadTimeout = 30000;
        public string START_URL = "https://google.com";
        public string DRIVER_EXE_FOLDER
        {
            get
            {
                var _rootPath = ExecutionDirectoryResolver.GetRootPath();
                return $"{_rootPath}\\DriverExecutables";
            }
        }

    }
}