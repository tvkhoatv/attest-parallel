﻿using ATTest.Settings;
using ATTest.Utilities;
using Microsoft.Extensions.Configuration;

namespace ATTest;

public sealed class ConfigurationService
{
    private static IConfigurationRoot _root;

    private static string _basePath { get; set; }

    static ConfigurationService()
    {
        _basePath = ExecutionDirectoryResolver.GetRootPath();
        _root = InitializeConfiguration();
    }

    public static TSection GetSection<TSection>()
      where TSection : class, new()
    {
        string sectionName = MakeFirstLetterToLower(typeof(TSection).Name);
        return _root.GetSection(sectionName).Get<TSection>();
    }

    private static string MakeFirstLetterToLower(string text)
    {
        return char.ToLower(text[0]) + text.Substring(1);
    }

    private static IConfigurationRoot InitializeConfiguration()
    {
        var projectDir = ExecutionDirectoryResolver.GetProjectPath();
        var environment = SettingRuntime.GetProperty("Environment");
        var filesInExecutionDir = Directory.GetFiles(projectDir);
        var builder = new ConfigurationBuilder().SetBasePath(projectDir);
        var settingsFile =
            filesInExecutionDir.FirstOrDefault(x => x.Contains("environment") && x.Contains(environment != "" ? "." + environment.ToString() : "") && x.EndsWith(".json"));

        if (settingsFile != null)
        {
            builder.AddJsonFile(settingsFile, optional: true, reloadOnChange: true);
        }

        builder.AddEnvironmentVariables();

        return builder.Build();
    }
}
