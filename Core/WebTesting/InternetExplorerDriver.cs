﻿using ATTest.Drivers;
using OpenQA.Selenium.IE;
using OpenQA.Selenium;

namespace ATTest.WebTesting
{
    public class InternetExplorerDriverWeb : IDrivers
    {
        
        public string? FOLDERPATH { get; set; }

        public object DesiredCapabilities
        {
            get
            {
                InternetExplorerOptions opts = new InternetExplorerOptions();
                opts.EnsureCleanSession = true;
                opts.RequireWindowFocus = false;
                opts.IgnoreZoomLevel = false;
                opts.IntroduceInstabilityByIgnoringProtectedModeSettings = false;
                opts.ElementScrollBehavior = InternetExplorerElementScrollBehavior.Top;
                return opts;
            }
        }

        public object? Driver { get; set; }

        public object DriverServices
        {
            get
            {
                InternetExplorerDriverService service = InternetExplorerDriverService.CreateDefaultService(FOLDERPATH);
                service.HideCommandPromptWindow = false;
                return service;
            }
        }

        public void InitDriver(Configuration config)
        {
            FOLDERPATH = config.DRIVER_EXE_FOLDER;
            config.DesiredCapabilities = config.DesiredCapabilities ?? DesiredCapabilities;
            config.DriverServices = config.DriverServices ?? DriverServices;
            IWebDriver driver = new InternetExplorerDriver((InternetExplorerDriverService)config.DriverServices, (InternetExplorerOptions)config.DesiredCapabilities, TimeSpan.FromSeconds(60));
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromMilliseconds(config.PageLoadTimeout);
            driver.Navigate().GoToUrl(config.START_URL);
            Driver = driver;
        }
    }
}
