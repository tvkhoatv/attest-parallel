﻿using ATTest.Drivers;
using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;

namespace ATTest.WebTesting
{
    public class FireFoxDriverWeb : IDrivers
    {
        public string? FOLDERPATH
        {
            get;set;
        }
        public object DesiredCapabilities
        {
            get
            {
                FirefoxOptions opts = new FirefoxOptions();
                opts.AcceptInsecureCertificates = true;
                opts.PageLoadStrategy = PageLoadStrategy.Eager;
                opts.AcceptInsecureCertificates = true;
                return opts;
            }
        }

        public object? Driver { get; set; }

        public object DriverServices
        {
            get
            {

                FirefoxDriverService service = FirefoxDriverService.CreateDefaultService(FOLDERPATH);
                service.SuppressInitialDiagnosticInformation = true;
                return service;
              
            }
        }

        public void InitDriver(Configuration config)
        {
            FOLDERPATH = config.DRIVER_EXE_FOLDER;
            config.DriverServices = config.DriverServices ?? DriverServices;
            config.DesiredCapabilities = config.DesiredCapabilities ?? DesiredCapabilities;
            IWebDriver driver = new FirefoxDriver((FirefoxDriverService)config.DriverServices, (FirefoxOptions)config.DesiredCapabilities, TimeSpan.FromSeconds(60));
            driver.Manage().Timeouts().AsynchronousJavaScript = TimeSpan.FromSeconds(10);
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromMilliseconds(config.PageLoadTimeout);
            driver.Navigate().GoToUrl(config.START_URL);
            Driver = driver;
        }
    }
}
